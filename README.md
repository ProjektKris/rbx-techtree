# rbx-techtree-renderer
a module used to build tech trees from lua tables in roblox

## Getting Started
To build the place from scratch, use:

```bash
rojo build -o "rbx-techtree-renderer.rbxlx"
```

Next, open `rbx-techtree-renderer.rbxlx` in Roblox Studio and start the Rojo server:

```bash
rojo serve
```

For more help, check out [the Rojo documentation](https://rojo.space/docs).