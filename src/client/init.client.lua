local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Players = game:GetService("Players")
local SCREEN_GUI = script.ui
local PLAYER = Players.LocalPlayer
local techtree = require(ReplicatedStorage.Common.techtree)
local techtreeconfig = require(ReplicatedStorage.Common.configs.techtreeconfig)
local techtreestyle = require(ReplicatedStorage.Common.configs.techtreestyle)

SCREEN_GUI.Parent = PLAYER.PlayerGui

local newTree = techtree.New(techtreeconfig, techtreestyle,{
    MouseButton1Down = function(instance)
        print(string.format('MouseButton1Down: %s', instance.Name))
    end
})
local success, result = pcall(function()
    newTree.Events.ButtonAdded.Event:Connect(function(button: TextButton, data: string)
        local newCorner: UICorner = Instance.new("UICorner")
        newCorner.CornerRadius = UDim.new(0, 2)
        newCorner.Parent = button
    end)
    newTree.Events.TitleAdded.Event:Connect(function(label: TextLabel, data)
        label.TextSize = 30
    end)
    newTree:Render(SCREEN_GUI.Frame)
end)
if not success then
    warn(result)
end