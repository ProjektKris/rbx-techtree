--!nocheck

--[[
	README.md

	# rbx-techtree-renderer

	a module used to build tech trees from lua tables in roblox

	it is very recommended that you adjust the code below manually for the visuals.
]]

local create = require(script.create)
local combinetable = require(script.combinetable)

type Array<T> = { [number]: T }
export type ContentOnEvents = { [string]: () -> () } -- dictionary containing functions
export type Stylesheet = {
	TitleSize: UDim2,
	Padding: UDim,
	ButtonHeight: UDim,
}
export type Tree = {
	Map: any,
	ContentOnEvents: ContentOnEvents,
	Stylesheet: Stylesheet,
	Frame: Frame,

	Events: {
		ButtonAdded: BindableEvent,
	},

	RenderRowContents: (Array<string>) -> Array<Instance | GuiObject>,
	RenderRow: (Array<Array<string>>) -> Array<Instance | GuiObject>,
	RenderColumns: () -> Array<Instance | GuiObject>,
	Render: (Instance) -> Frame,
}

local tree = {}

function tree.New(map: any, stylesheet: Stylesheet, contentOnEvents: ContentOnEvents): Tree
	local newTree: Tree = {
		Map = map,
		ContentOnEvents = contentOnEvents, -- basically generic function
		Stylesheet = stylesheet,
		Frame = nil,

		Events = {
			TitleAdded = Instance.new("BindableEvent"),
			ColumnAdded = Instance.new("BindableEvent"),
			RowAdded = Instance.new("BindableEvent"),
			ButtonAdded = Instance.new("BindableEvent"),
		},
	}

	-- methods
	function newTree:GetMaxRowContent(data: Array<Array<string>>): number
		--[[
			gets the maximum amount of content in a row of a column
			can be used to identify column width automatically

			data is map[colName].Value
		]]
		local max = 0
		for _, vehicles in ipairs(data) do
			if #vehicles > max then
				max = #vehicles
			end
		end
		-- print(max)
		return max
	end

	function newTree:GetMaxColTile(): number
		--[[
			
		]]
		local max = 0
		for _, v in ipairs(self.Map) do
			max = max + self:GetMaxRowContent(v.Value)
		end
		-- print(max)
		return max
	end

	function newTree:RenderRowContents(data: Array<string>): Array<Instance | GuiObject>
		-- data -> the vehicles list in said tier/row
		local instances: Array<Instance | GuiObject> = {}
		for i, v in pairs(data) do
			instances[i] = create("TextButton", {
				Name = v,
				ZIndex = 2,
				Size = UDim2.new(
					UDim.new(1 / #data) - self.Stylesheet.ButtonPadding,
					UDim.new(1, 0) - self.Stylesheet.ButtonPadding
				), --UDim2.new(1 / #data, 0, 1, 0), -- will do something about this
				LayoutOrder = i,
				Text = v,
			})
			for eventName: string, fn: () -> () in pairs(self.ContentOnEvents) do
				-- not protected: it should be the developer's job to make sure that he doesn't add in wrong eventName, this shouldn't pass through production
				local signal: RBXScriptSignal = instances[i][eventName]
				signal:Connect(function(...)
					fn(instances[i], ...)
				end)
			end
			self.Events.ButtonAdded:Fire(instances[i], v)
		end
		return instances
	end

	function newTree:RenderRow(data: Array<Array<string>>): Array<Instance | GuiObject>
		-- data -> .Value
		local instances: Array<Instance | GuiObject> = {}
		for i, v in pairs(data) do
			instances[i] = create("Frame", {
				Name = string.format("row-%i", i),
				Size = UDim2.new(UDim.new(1, 0), self.Stylesheet.RowHeight), -- will do something about this
				BackgroundTransparency = 1,
				LayoutOrder = i,
				_Children = combinetable.CombineArr({
					create("UIListLayout", {
						FillDirection = Enum.FillDirection.Horizontal,
						HorizontalAlignment = Enum.HorizontalAlignment.Center,
						VerticalAlignment = Enum.VerticalAlignment.Center,
						SortOrder = Enum.SortOrder.LayoutOrder,
						Padding = self.Stylesheet.ButtonPadding,
					}),
				}, self:RenderRowContents(
					v
				)),
			})
			self.Events.RowAdded:Fire(instances[i], v)
		end
		return instances
	end

	function newTree:RenderColumns(): Array<Instance | GuiObject>
		local columnInstances = {}

		-- loop through the columns data
		for i, v in ipairs(self.Map) do
			local title = create("TextLabel", {
				Name = "Title",
				LayoutOrder = 0,
				Size = self.Stylesheet.TitleSize,
				BackgroundTransparency = 1,
				Text = v.Name,
			})
			columnInstances[i] = create("Frame", {
				Name = string.format("col-%s", v.Name),
				Size = UDim2.new(
					UDim.new(self:GetMaxRowContent(v.Value) / self:GetMaxColTile(), 0) - self.Stylesheet.ColumnPadding,
					UDim.new(1, 0)
				), --v.Size,--UDim2.new(1 / #self.Map, 0, 1, 0),
				BackgroundTransparency = 1,
				LayoutOrder = i,
				_Children = combinetable.CombineArr({
					create("UIListLayout", {
						FillDirection = Enum.FillDirection.Vertical,
						SortOrder = Enum.SortOrder.LayoutOrder,
					}),
					title,
				}, self:RenderRow(
					v.Value
				)),
			})
			self.Events.TitleAdded:Fire(title, v)
			self.Events.ColumnAdded:Fire(columnInstances[i], v)
		end

		return columnInstances
	end

	function newTree:RenderColumnDecos(): Array<Instance | GuiObject>
		local columnInstances = {}

		-- loop through the columns data
		for i, v in ipairs(self.Map) do
			columnInstances[i] = create("Frame", {
				Name = string.format("col-%s", v.Name),
				Size = UDim2.new(
					UDim.new(self:GetMaxRowContent(v.Value) / self:GetMaxColTile(), 0) - self.Stylesheet.ColumnPadding,
					UDim.new(1, 0)
				), --v.Size,--UDim2.new(1 / #self.Map, 0, 1, 0),
				BackgroundTransparency = 1,
				LayoutOrder = i,
				_Children = {
					create("Frame", {
						Name = "line",
						Size = UDim2.new(self.Stylesheet.ColumnLineWidth, UDim.new(1, 0)),
						AnchorPoint = Vector2.new(0.5, 0),
						Position = UDim2.new(UDim.new(0.5, 0), self.Stylesheet.TitleSize.Y),
						BorderSizePixel = 0,
						BackgroundTransparency = 0.5,
					}),
				},
			})
		end

		return columnInstances
	end

	function newTree:Render(parent: Instance): Frame
		-- create the primary container
		local newFrame: Frame = create("Frame", {
			Name = "techtree",
			Parent = parent,
			Size = UDim2.new(1, 0, 1, 0),
			BackgroundTransparency = 1,
			_Children = {
				create("Frame", {
					Name = "columns",
					Size = UDim2.new(1, 0, 1, 0),
					BackgroundTransparency = 1,
					ZIndex = 1,
					_Children = combinetable.CombineArr({
						create("UIListLayout", {
							FillDirection = Enum.FillDirection.Horizontal,
							SortOrder = Enum.SortOrder.LayoutOrder,
							HorizontalAlignment = Enum.HorizontalAlignment.Center,
							Padding = self.Stylesheet.ColumnPadding,
						}),
					}, self:RenderColumns()),
				}),
				create("Frame", {
					Name = "deco",
					Size = UDim2.new(1, 0, 1, 0),
					BackgroundTransparency = 1,
					ZIndex = 0,
					_Children = combinetable.CombineArr({
						create("UIListLayout", {
							FillDirection = Enum.FillDirection.Horizontal,
							SortOrder = Enum.SortOrder.LayoutOrder,
							HorizontalAlignment = Enum.HorizontalAlignment.Center,
							Padding = self.Stylesheet.ColumnPadding,
						}),
					}, self:RenderColumnDecos()),
				}),
			},
		})

		self.Frame = newFrame
		return newFrame
	end

	return newTree
end

return tree
