--[[
	# combinetable.lua

	a library designed to help with joining/merging/combining tables
]]
local combine = {}

function combine.CombineDictionary(table0, table1)
	-- table1 overwrites table0 if it has same index
	local newTable = { table.unpack(table0) }
	for i, v in pairs(table1) do
		newTable[i] = v
	end
	return newTable
end

function combine.CombineArr(table0, table1)
	local newTable = { table.unpack(table0) }
	for _, v in pairs(table1) do
		newTable[#newTable + 1] = v
	end
	return newTable
end

return combine
