--[[
	# create.lua

	a function designed to make complex instancing easy and shorter (doesn't improve performance)

	for 3d instances it's still good practice to assign the Parent property 
	outside the properties table parameter (this way it might not require to 
	re-render multiple times due to properties changing i.e. material)

	## Example

	```lua
	local create = require(create)
	local newFrame = create("Frame", {
		Parent = parent, -- having it on top doesn't guarrantee that it will be set first
		Size = UDim2.new(1, 0, 1, 0),
		_Children = {
			create("UIListLayout", {
				FillDirection = Enum.FillDirection.Horizontal,
				SortOrder = Enum.SortOrder.LayoutOrder,
			}),
		},
	})
	```
]]

local function writeProperties(newInstance, p)
	for propertyName, value in pairs(p) do
		if propertyName == "_Children" then
			if type(value) == "table" then
				for _, child in pairs(value) do
					child.Parent = newInstance
				end
			else
				value.Parent = newInstance
			end
		else
			newInstance[propertyName] = value
			-- if newInstance[propertyName] then
			-- else
			-- 	warn(string.format("Property %s not found in instance %s", propertyName, instanceType))
			-- end
		end
	end
end
return function(instanceType, properties, template)
	-- create a new instance
	local newInstance = Instance.new(instanceType)

	-- loop through the template
	if template ~= nil then
		writeProperties(newInstance, template)
	end

	-- loop through the property table; overwrites template (except _Children property)
	writeProperties(newInstance, properties)

	return newInstance
end
