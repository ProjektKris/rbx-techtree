return {
	{
		Name = "Light",
		Value = {
			[1] = {
				"pz2c",
			},
		},
	},
	{
		Name = "Medium",
		Value = {
			[1] = {
				"pz35t",
				"pz4c",
			},
		},
	},
	{
		Name = "Heavy",
		Value = {
			[1] = {
				"tiger1",
			},
		},
	},
	{
		Name = "TD",
		Value = {
			[1] = {
				"pzjg1",
			},
		},
	},
}
