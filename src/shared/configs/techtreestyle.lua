return {
	TitleSize = UDim2.new(1, 0, 0, 50),
	RowHeight = UDim.new(0, 60),
	ColumnPadding = UDim.new(0, 30),
	ButtonPadding = UDim.new(0, 30),
	ColumnLineWidth = UDim.new(0, 10),
}
